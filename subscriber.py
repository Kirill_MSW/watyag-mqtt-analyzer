import paho.mqtt.client as mqtt



class Subscriber:
	def __init__(self,MQTT_HOST,MQTT_PORT,subscriber_id):
		self.MQTT_HOST=MQTT_HOST
		self.MQTT_PORT=MQTT_PORT
		self.MQTT_KEEPALIVE_INTERVAL = 5
		self.client=mqtt.Client(subscriber_id)

	def subscribe(self,MQTT_TOPIC):
		self.MQTT_TOPIC=MQTT_TOPIC
		self.client.subscribe(MQTT_TOPIC)

MQTT_HOST = "iot.eclipse.org"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 5
MQTT_TOPIC = "SampleTopic"
MQTT_MSG = "Hello MQTT"

clie=Subscriber('192.168.199.9',43,1)
clie.subscribe('house/temerature')


try:
	clie.client.connect(clie.MQTT_HOST, clie.MQTT_PORT, 5)
	print(clie.client.on_connect())
	clie.client.disconnect()
except:
	print("connection failed")
	exit(1)
