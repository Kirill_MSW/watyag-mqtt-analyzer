import socket
import threading

bind_ip = '0.0.0.0'
bind_port = 1243
max_connections = 5 #edit this to whatever number of connections you need

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(max_connections)  # max backlog of connections

print (('Listening on {}:{}').format(bind_ip, bind_port))

def check_n_send(data):
    #checking
    try:
        broker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        broker.connect(('192.168.199.8',9095))
        broker.send(data)
        broker.close()
    except:
        print("SENDING DATA ERROR")



def handle_client_connection(client_socket):
    try:
        request = client_socket.recvfrom(4096 )
        print(request)
        check_n_send(request[0])
        client_socket.close()
    except:
        print("GETTING DATA ERROR")

while True:
    try:
        client_sock, address = server.accept()
    except:
        print("CONNECTION ERROR")
        break
    print (('Accepted connection from {}:{}').format(address[0], address[1]))
    client_handler = threading.Thread(
        target=handle_client_connection,
        args=(client_sock,)
    )
    client_handler.start()
