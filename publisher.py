import paho.mqtt.client as paho

broker_host = "192.168.199.9"
broker_port = 1243
topic_name = 'house/temperature'
topic_value = "20"

client = paho.Client()

def on_connect(client, userdata, flags, rc):
   if rc == 0:
      print("connected ok")

def on_publish(client, userdata, mid):
    print("Topic published")

client.on_connect = on_connect
client.on_publish = on_publish

try:
    client.connect(broker_host, broker_port, 5)
    client.publish(topic_name, topic_value)
    client.disconnect()
except:
    print("connection failed")
    exit(1)
