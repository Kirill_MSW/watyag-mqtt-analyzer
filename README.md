# WATyag MQTT analyzer

This is the tool to analyze MQTT/UDP traffic that is have a wide spread in smart
homes. 

Traffic validation happens inside proxy.

We need subscriber, broker, publisher in order to simulate smart home network.

Includes: subscriber, broker, publisher, proxy (validation)